workspace {
    model {
        bankUser = person "Customer" "User have account shopping"
        bankAdmin = person "Admin" "site administrator"
        eComm = softwareSystem "Online Sales System" "Display for sale,takes orders"
        Fulfilment = softwareSystem "Fulfilment System" "Fulfiment system that tracks inventory and ship order to customers"
        Payment = softwareSystem "Payment Gateway" "Provides payment service" 
    // systemContext
        bankUser -> eComm "Orders using"
        Fulfilment -> bankUser "ships"
        Fulfilment -> bankUser "Emails"
        Fulfilment -> eComm "list of products sold"
        eComm -> Payment "Redirects Payment to"
        eComm -> Fulfilment "Sends order to"
        bankAdmin -> Fulfilment "manage"
        eComm -> bankAdmin "Product handling"
        payment -> bankAdmin "Save history"



        // bankAdmin -> eComm "managee"

        
    //Container
    softwareSystem = softwareSystem "Online Sales System." {
            spa = container "SPA Home" "Run application code in customer's browser" " Vuejs/Javascript"  {
                bankUser -> this "login, add products to cart and purchase using"
               
            }
            admin = container "API Administration" "Admin tool for system configuration" "ASP.Net core,C#"{
                Controller1 = component "Controller" "Provides API interface checks token validity and allows valid user to peruse and purchase product via API" "ASP.net core"{
                    
                }
                  Service1 = component "Service" "Business logic for displaying product ,shopping basket and placing orders,Integrates to Fulfiment System" "ASP.net core rest Service"{
                    Controller1 -> this "User"
                  }
                
                componentAdminLogin = component "Admin Model" "Provide data model of login user and interface to database" "ASP.net Core Rest Model"{
                    Service1 -> this "User"
                }
                componentAdminLogin1 = component "Admin login Model" "Provide cached data model of login by user" "ASP.net Core Rest Model"{
                    Service1 -> this "User"
                }
                Admin1 = component "Manage Product List Model" "Provide cached data add product and product repair" "ASP.net Core Rest Model"{
                    Service1 -> this "User"
                }
                Admin2 = component "product type Model" "Provide cached data more product type,edit product type" "ASP.net Core Rest Model"{
                    Service1 -> this "User"
                }
            }
            website = container "SPA Admin" "Server static website assets to customer" "Apache"{
                
                bankAdmin -> this "build, administer, organize images and articles on the web"

            }
            
            api = container "API Home page" "Exposes api that supports user registration and cart status still exists" "ASP.Net core,C#" {
                spa -> this "registers new customer using [JDBC]"
                spa -> this "retrieves listing and plances orders using [JDBC]"
                 Controller = component "Controller" "Provides API interface checks token validity and allows valid user to peruse and purchase product via API" "ASP.net core rest controller"{
                }

                Service = component "Service" "Business logic for displaying product ,shopping basket and placing orders,Integrates to Fulfiment System" "ASP.net core rest Service"{
                    Controller -> this "User"   
                }
                CPM1 = component "Shopping Basket Model" "Provide data model of shopping basket and interface to database" "ASP.net Core Rest Model"{
                    Service -> this "User"
                }
                CPM2 = component "Orders Model" "Provide cached data model of orders by customer" "ASP.net Core Rest Model"{
                    Service -> this "User"
                }
                CPM3 = component "Listings Model" "Provide cached data model of listings" "ASP.net Core Rest Model"{
                    Service -> this "User"
                }
                ManageShopping = component "Login Shopping Model" "Provide functionality relates to signing in,chaning passwords,etc." "ASP.net Core Rest Model"{
                    Service -> this "User"
                }
            }
            container "shopping database user" "Stores shopping cart state and history" "SQL Server" "Cylinder" {
                api -> this "Reads from and writes [JDBC]"
                CPM1 -> this "Reads from and writes [JDBC]"
                
            }
            container "Configuration Database" "Contais configuration Setting" "SQL Server" "Cylinder" {
                admin -> this "Reads and updetes configuration settings [JDBC]"
                api -> this "Reads configuration data [JDBC]"
                ManageShopping -> this "Reads from and writes [JDBC]"
                // api -> admin "Read and updates configuration setting [JDBC]"
                componentAdminLogin -> this "Read from and Write to[JDBC]"
                Admin1 -> this "Read from and Write to[JDBC]"
                Admin2 -> this "Read from and Write to[JDBC]"
            }
            service -> Fulfilment "Retrieves listlings[JSON/HTTPS]"
            service -> Fulfilment "Sends message to place order using [JSON/HTTPS]"
            api -> Fulfilment "Retrieves listlings[JSON/HTTPS]"
            api -> Fulfilment "Sends message to place order using [JSON/HTTPS]"
            spa -> Payment "Makes payment using[JSON/HTTPS]"
            website -> admin "Makes api calls to[JSON/HTTPS]"
            spa -> Controller "retrieves listings/basket/orders and places orders using"
            website -> Controller1 "admin login/list of products/product type"
        }

    }
    views {
         systemContext eComm "ecommSystem"  {
            include *
            autoLayout
        }

         systemContext Fulfilment "FulfilmenSystem"  {
            include *
            autoLayout
        }

         systemContext Payment "PaymentSystem"  {
            include *
            autoLayout
        } 
         container softwareSystem "Container" {
            include *
            autolayout lr
        }
        component api "ComponentHome" {
            include *
            autolayout lr
        }
        component admin "ComponentAdmin" {
            include *
            autolayout lr
        }

        
        styles {
            element "Software System" {
                background #1168BD
                color #ffffff
            }
            element "Person" {
                shape person
                background #08427b
                color #ffffff
            }
            element "container"{
                background #438DD5
                color #ffffff

            }
            element "component" {
                background #438DD5
                color #FFFFFF
            }
            element Cylinder {
                shape "Cylinder"
                background #1168bd
                color #ffffff
            }
        }
    }
}